import { Component, Input, OnInit } from '@angular/core';
import { PokemonOverview } from 'src/app/models/pokemon-overview.model';

@Component({
  selector: 'app-pokemon-list-item',
  templateUrl: './pokemon-list-item.component.html',
  styleUrls: ['./pokemon-list-item.component.css']
})
export class PokemonListItemComponent implements OnInit {

  @Input() pokemon!: PokemonOverview;

  constructor() { }

  ngOnInit(): void {
  }
}
