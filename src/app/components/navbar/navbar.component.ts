import { Component} from '@angular/core';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent{

  constructor(private readonly userService: UserService) { }

  get user(): User | undefined{
    return this.userService.User
  }

  //When log out is click, remove user from session storage and reload page to redirect
  public logOut(){
    this.userService.logoutUser()
    window.location.reload()
  }


}
