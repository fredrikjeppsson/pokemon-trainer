import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { CollectedPokemonService } from 'src/app/services/collected-pokemon.service';
import { UserService } from 'src/app/services/user.service';
import { POKEMON_EMPTY_BALL, POKEMON_FILLED_BALL } from 'src/app/utils/constants';

@Component({
  selector: 'app-catch-button',
  templateUrl: './catch-button.component.html',
  styleUrls: ['./catch-button.component.css']
})
export class CatchButtonComponent implements OnInit {

  @Input() pokemonId: string = ""
  @Input() pokemonName: string = ""
  private _pokemonEmpty: string = POKEMON_EMPTY_BALL
  private _pokemonFilled: string = POKEMON_FILLED_BALL

  public _isCaught: boolean = false

  constructor(private readonly collectedService: CollectedPokemonService, private readonly userService: UserService) { }

  ngOnInit(): void {
    this._isCaught = this.userService.pokemonCaught(this.pokemonName)
  }


  //States for the button image
  get pokeballCaughtImage(): string {
    return this._pokemonFilled;
  }

  get pokeballNotCaughtImage(): string {
    return this._pokemonEmpty;
  }



  //When the catch button is clicked, the handleCollectedPokemon will run
  //and depending on current status of the pokemon will either add or remove from the users list
  onBallClick(): void {

    this.collectedService.handleCollectedPokemon(this.pokemonId).subscribe({
      next: () => {
        this._isCaught = this.userService.pokemonCaught(this.pokemonName)
      },
      error: (error) => { 
        throw new Error(error.message)
      }
    })
  }

}
