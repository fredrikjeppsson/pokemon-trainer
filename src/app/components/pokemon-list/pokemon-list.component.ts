import { Component, Input, OnInit } from '@angular/core';
import { PokemonOverview } from 'src/app/models/pokemon-overview.model';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent{

  constructor() { }

  @Input() pokemons: PokemonOverview[] = []

}
