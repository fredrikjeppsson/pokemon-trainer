import { Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { LoginService } from 'src/app/services/login.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent{

  @Output() loggedIn: EventEmitter<void> = new EventEmitter()

  constructor( private readonly loginService: LoginService, private readonly userService: UserService) { }

  handleLogin(form: NgForm): void{
    
    const { trainerName } = form.value

    //Provides the login service with the username/trainer name and when its done
    //sets the current user to the retrieved.
    this.loginService.login(trainerName)
      .subscribe({
        next: (user: User) => {
          this.userService.User = user
          //Notifies the parent about the login event (for redirection)
          this.loggedIn.emit()
        },
        error: (error) => {
          
        }
      })   
  }
}
