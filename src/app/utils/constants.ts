export const POKEMON_IMAGE_BASE_URL = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon'
export const POKEMON_LOCAL_STORAGE_KEY = 'POKEMONS_LOCAL_STORAGE'
export const POKEMON_FILLED_BALL = 'assets/images/pokeball_filled.png'
export const POKEMON_EMPTY_BALL = 'assets/images/pokeball_empty.png'