import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PokemonOverview } from '../models/pokemon-overview.model';
import { User } from '../models/user.model';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { UserService } from './user.service';

const { trainerApiUrl, trainerApiKey } = environment

@Injectable({
  providedIn: 'root'
})
export class CollectedPokemonService {

  constructor(
    private readonly userService: UserService, 
    private readonly pokemonCatalogue: PokemonCatalogueService,
    private readonly http: HttpClient,
    ) { }
  

  //Gets the pokemon array from CatalogueService and filters it with the caught pokemon array from the user service
  public loadCollected(): PokemonOverview[]{
    return this.pokemonCatalogue.pokemons.filter(pokemon => this.userService.pokemonCaught(pokemon.name))
  }
  

  //Checks if user
  public handleCollectedPokemon(pokemonId: string): Observable<User>{
    if(!this.userService.User) throw new Error("User undefined")

    const pokemonObj = this.pokemonCatalogue.pokemonById(pokemonId)
    if(!pokemonObj) throw new Error("No pokemon with that id!")
    const isCaught = this.userService.pokemonCaught(pokemonObj.name)

    if(isCaught) this.userService.removePokemonFromCaught(pokemonObj.name)
    else if(!isCaught) this.userService.addPokemonToCaught(pokemonObj.name)

    

    const pokemon: string[] = [...this.userService.User.pokemon]

    const user = {
      username: this.userService.User.username,
      pokemon,
    }

    const headers = new HttpHeaders({
      "content-type": "application/json",
      "x-api-key": trainerApiKey
    })

    return this.http.patch<User>(`${trainerApiUrl}/${this.userService.User.id}`, user, {headers}).pipe(
      tap((updatedUser: User) => this.userService.User = updatedUser)
    )

  }


}
