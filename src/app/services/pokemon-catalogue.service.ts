import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PokemonOverview } from '../models/pokemon-overview.model';
import { PokemonResponse } from '../models/pokemon-response';
import { StorageUtil } from '../utils/storage.util';
import { POKEMON_LOCAL_STORAGE_KEY, POKEMON_IMAGE_BASE_URL } from '../utils/constants'

const { pokemonApiUrl } = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {
  private _pokemons: PokemonOverview[] = [];
  private _pokemonResponse: PokemonResponse | null = null;
  private _loading: boolean = false;

  constructor(private readonly http: HttpClient) { }

  get pokemons(): PokemonOverview[] {
    return this._pokemons
  }

  public getPokemonOverview(): void {
    const pokemons = StorageUtil.localStorageRead<PokemonOverview[]>(POKEMON_LOCAL_STORAGE_KEY)
    if (pokemons) {
      this._pokemons = pokemons;
      return;
    }

    if (this._pokemons.length > 0 || this._loading) {
      return;
    }

    this._loading = true;

    this.http.get<PokemonResponse>(pokemonApiUrl)
      .pipe(
        finalize(() => this._loading = false)
      )
      .subscribe({
        next: (pokemonResponse: PokemonResponse) => {
          // saving the raw response for the time being as well. possibly redundant.
          this._pokemonResponse = pokemonResponse
          this._pokemons = this.parsePokemons(pokemonResponse);
          StorageUtil.localStorageSave(POKEMON_LOCAL_STORAGE_KEY, this._pokemons);
        },
        error: (error: HttpErrorResponse) => {
          console.log(error);
        }
      })
  }

  private parsePokemons(pokemonResponse: PokemonResponse): PokemonOverview[] {
    const pokemons = [];
    for (const { name, url } of pokemonResponse.results) {
      // id is found at the end of the url.
      const id = url.substring(34, url.length - 1)
      const pokemon = {
        id,
        name,
        image: `${POKEMON_IMAGE_BASE_URL}/${id}.png`
      }
      pokemons.push(pokemon);
    }
    return pokemons;
  }

  public pokemonById(id: string): PokemonOverview | undefined{
    return this._pokemons.find((pokemon: PokemonOverview) => pokemon.id === id)
  }


}


