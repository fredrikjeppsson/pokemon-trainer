import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from 'src/environments/environment';
import { map, Observable, of, switchMap, tap } from 'rxjs';
import { User } from '../models/user.model';




const { trainerApiUrl, trainerApiKey } = environment

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private readonly http: HttpClient) { }

  public login(trainerName: string): Observable<User>{
    
    /*checkTrainer returns a user or undefined switch man is then used to either return
    the user from checkTrainer or create and return a train if the trainer did not already exist */
    return this.checkTrainer(trainerName)
      .pipe(
        switchMap((user: User | undefined) => {
          if(user === undefined) return this.createTrainer(trainerName)
          return of(user) //Returns an Observable User
        })
      )
  }


  private checkTrainer(trainerName: string) : Observable<User | undefined>{
      //api returns array which contains the user object pop() to get the user
      return this.http.get<User []>(`${trainerApiUrl}?username=${trainerName}`)
        .pipe(
          map((response: User[]) => response.pop())
        )
  }

  private createTrainer(username: string) : Observable<User>{
      const user = {
        username,
        pokemon: []
      }

      const headers = new HttpHeaders({
        "content-type": "application/json",
        "x-api-key": trainerApiKey
      })

      //Post request will return the new User
      return this.http.post<User>(trainerApiUrl, user, {headers})
    
    
  }

}
