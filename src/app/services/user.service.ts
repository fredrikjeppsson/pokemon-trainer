import { Injectable } from '@angular/core';
import { StorageKey } from '../enums/storage-key.enum';
import { PokemonOverview } from '../models/pokemon-overview.model';
import { User } from '../models/user.model';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _user?: User

  constructor() {
    this._user = StorageUtil.sessionStorageRead<User>(StorageKey.User)
  }

  get User(): User | undefined {
    return this._user
  }

  set User(user: User | undefined) {
    StorageUtil.sessionStorageSave<User>(StorageKey.User, user!)
    this._user = user
  }

  //Logs out user by removing from storage
  public logoutUser(): void{
    StorageUtil.sessionStorageRemove(StorageKey.User)
  }

  //Checks if a pokemon is caught by comparing the users pokemon array with the name in the parameter
  public pokemonCaught(pokemonName: string): boolean{
    if(this._user) return Boolean(this._user?.pokemon.find((pokemon: string) => pokemon === pokemonName))
    return false
  }

  //Adds new pokemon to the users pokemon array
  public addPokemonToCaught(pokemonName: string): void {
    if(this._user) this._user?.pokemon.push(pokemonName)
  }

  //Filters out the pokemon to be removed from the users array
  public removePokemonFromCaught(pokemonName: string): void{
    if(this._user) this._user.pokemon = this._user.pokemon.filter(pokemon => pokemon !== pokemonName)
  }


}
