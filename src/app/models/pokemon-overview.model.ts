export interface PokemonOverview {
    id: string,
    name: string,
    image: string
}