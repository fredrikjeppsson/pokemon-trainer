// Response from Pokemon API when called with
// only https://pokeapi.co/api/v2/pokemon
export interface PokemonResponse {
    count: number;
    next: string;
    previous?: any;
    results: Result[];
}

export interface Result {
    name: string;
    url: string;
}