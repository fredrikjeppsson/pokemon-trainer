import { PokemonOverview } from "./pokemon-overview.model";


export interface User {
    id: number,
    username: string,
    pokemon: string[],
}