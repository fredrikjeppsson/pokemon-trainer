import { Component, OnInit } from '@angular/core';
import { PokemonCatalogueService } from './services/pokemon-catalogue.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  constructor(private readonly catalogueService : PokemonCatalogueService){

  }
  title = 'ng-pokemon-trainer';

  ngOnInit(): void {
    this.catalogueService.getPokemonOverview();
  }

}
