import { Component, OnInit } from '@angular/core';
import { PokemonOverview } from 'src/app/models/pokemon-overview.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.css']
})
export class PokemonCataloguePage{

  constructor(private readonly catalogueService : PokemonCatalogueService) { }


  get pokemons(): PokemonOverview[] {
    return this.catalogueService.pokemons;
  }

  
}
