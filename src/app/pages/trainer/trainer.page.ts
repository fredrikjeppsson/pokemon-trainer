import { Component, OnInit } from '@angular/core';
import { PokemonOverview } from 'src/app/models/pokemon-overview.model';
import { User } from 'src/app/models/user.model';
import { CollectedPokemonService } from 'src/app/services/collected-pokemon.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage{

  constructor(private readonly userService: UserService, private readonly pokemonCollectionService : CollectedPokemonService) { }

  get user(): User | undefined{
    return this.userService.User
  }

  //Gets the collected pokemon
  get pokemonCaught(): PokemonOverview[]{
    return this.pokemonCollectionService.loadCollected()
  }

}
