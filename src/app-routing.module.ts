import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./app/guards/auth.guard";
import { LoginGuard } from "./app/guards/login.guard";
import { LoginPage } from "./app/pages/login/login.page";
import { PokemonCataloguePage } from "./app/pages/pokemon-catalogue/pokemon-catalogue.page";
import { TrainerPage } from "./app/pages/trainer/trainer.page";


const routes: Routes = [
    {
        path: "",
        pathMatch: "full",
        redirectTo: "/login"
    },
    {
        path: "login",
        component: LoginPage,
        canActivate: [ LoginGuard ],
    },
    {
        path: "pokemons",
        component: PokemonCataloguePage,
        canActivate: [ AuthGuard ],
    },
    {
        path: "trainer",
        component: TrainerPage,
        canActivate: [ AuthGuard ],
    }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule    // exported with the route information we've added.
    ]
})
export class AppRoutingModule {
}