# Pokemon Trainer

Java Fullstack – Experis – July 2022

Assignment 3: Frontend Development with Angular

An Angular app that lets you view and catch pokemons.

![Pokemons](pokemon_catalogue.png)

## Authors

- Christoffer Malmberg
- Fredrik Jeppsson

## Component Tree

![Component Tree](pokemon_component_tree.png)

## Installation

- Clone repo.
- Change directory to project root.
- npm install 
- ng serve to run dev server

The app relies on the [Noroff API](https://github.com/dewald-els/noroff-assignment-api). If running locally, add the environments/environment.ts file with the following information.

```typescript
export const environment = {
  production: false,
  pokemonApiUrl: "https://pokeapi.co/api/v2/pokemon?limit=251",
  trainerApiUrl: <url to noroff api>,
  trainerApiKey: <api key to noroff api>,
};
```

## Attributions

<a href="https://www.flaticon.com/free-icons/pokemon" title="pokemon icons">Pokemon icons created by Darius Dan - Flaticon</a>

<a href="https://www.cdnfonts.com/pokemon-solid.font" title="pokemon font">Pokemon font by Cameral Dias</a>

